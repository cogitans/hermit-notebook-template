/**
 * Created by josuah on 28/08/16.
 */
const path = require("path");
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    context: __dirname,

    entry: path.join(__dirname, "src", "index.tsx"),

    output: {
        path: path.join(__dirname, "build"),
        filename: "index.js"
    },

    resolve: {
        extensions: [".js", ".jsx", ".ts", ".tsx"]
    },

    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: [/node_modules/],
                loader: "babel-loader"
            },
            {
                test: /\.tsx?$/,
                exclude: [/node_modules/],
                loader: "awesome-typescript-loader"
            }
        ]
    },

    devServer: {
        contentBase: path.join(__dirname, "build"),
        host: "0.0.0.0"
    },

    plugins: [
        new CopyWebpackPlugin([
            {
                from: path.join(__dirname, "static_files", "index.html")
            },
            {
                from: path.join(__dirname, "static_files", "css")
            }
        ])
    ]
};